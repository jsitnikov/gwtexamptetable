package ru.jsitnikov.exampletable.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Created by jacob on 02.03.17.
 */
public class Contact implements IsSerializable {
    private String firstName;
    private String lastName;
    private String urlUserPic = "http://ecm-journal.ru/images/design/userpic-default-small.svg";

    public Contact(String firstName) {
        this.firstName = firstName;
    }

    public Contact(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Contact() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUrlUserPic() {
        return urlUserPic;
    }

    public void setUrlUserPic(String urlUserPic) {
        this.urlUserPic = urlUserPic;
    }
}
