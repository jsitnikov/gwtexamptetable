package ru.jsitnikov.exampletable.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import ru.jsitnikov.exampletable.client.ui.component.column.ImageColumn;
import ru.jsitnikov.exampletable.client.ui.component.column.SimpleTextColumn;
import ru.jsitnikov.exampletable.client.ui.component.column.TextColumn;
import ru.jsitnikov.exampletable.client.ui.component.table.ExampleTable;
import ru.jsitnikov.exampletable.client.ui.component.table.IKeyModel;
import ru.jsitnikov.exampletable.shared.Contact;

import java.util.ArrayList;
import java.util.List;

public class ExampleTableEntryPoint implements EntryPoint {

    private ExampleTable table;

  @Override
  public void onModuleLoad() {
      TextColumn textColumn = new TextColumn<Contact>() {
          @Override
          public Widget fillHeaderCell(Label widget, String title) {
              widget.setText(title);
              return widget;
          }

          @Override
          public Widget fillCell(Label widget, Contact model) {
              widget.setText(model.getFirstName());
              return widget;
          }
      };

      SimpleTextColumn<Contact> simpleTextColumn = new SimpleTextColumn<Contact>() {
          @Override
          protected Widget fillCell(Label widget, Contact model) {
              widget.setText(model.getLastName());
              return widget;
          }
      };


      List<Contact> listContacts = new ArrayList<>();
      listContacts.add(new Contact("Вася"));
      listContacts.add(new Contact("Петя", "Петухов"));
      listContacts.add(new Contact("Маша", "Медведева"));
      listContacts.add(new Contact("Наташа"));
      listContacts.add(new Contact("Даша"));


      IKeyModel<Contact> KEY_MODEL = new IKeyModel<Contact>() {
          @Override
          public Object getKey(Contact item) {
              //FIXME: Уникальный в рамках примера но в продакшене должен быть уникальный
              return item.getFirstName();
          }
      };
      table = new ExampleTable<Contact>(KEY_MODEL);

      table.addColumn(textColumn, "First Name");
      table.addColumn(simpleTextColumn, "Last Name");
      table.addColumn(getImageColumn(), " ");

      table.setData(listContacts);

      RootPanel.get().add(table);

      listContacts.add(new Contact("Юля"));
      table.refresh(listContacts);
      table.refreshRow(new Contact("Гена", "Крокодил")); // Не появится в таблице так как такой строки там нет
      table.addRow(new Contact("Гена", "Крокодил2"));
  }

  private ImageColumn getImageColumn() {
      return new ImageColumn<Contact>() {

          @Override
          protected void addHandlerCell(Image widget, final Contact model) {
              widget.addClickHandler(new ClickHandler() {
                  @Override
                  public void onClick(ClickEvent clickEvent) {
                      final DialogBox dialogBox = new DialogBox();
                      FlexTable tableDB = new FlexTable();

                      final TextBox firstName = new TextBox();
                      firstName.setText(model.getFirstName());

                      final TextBox lastName = new TextBox();
                      lastName.setText(model.getLastName());

                      tableDB.setWidget(0,0, firstName);
                      tableDB.setWidget(1,0, lastName);

                      Button save = new Button("Сохранить");
                      save.addClickHandler(new ClickHandler() {
                          @Override
                          public void onClick(ClickEvent clickEvent) {
                              model.setFirstName(firstName.getValue());
                              model.setLastName(lastName.getValue());
                              getTable().refreshRow(model);
                              dialogBox.hide();
                          }
                      });

                      Button cancel = new Button("Отмена");
                      cancel.addClickHandler(new ClickHandler() {
                          @Override
                          public void onClick(ClickEvent clickEvent) {
                             dialogBox.hide();
                          }
                      });
                      tableDB.setWidget(2,0, save);
                      tableDB.setWidget(2,1, cancel);

                      dialogBox.add(tableDB);
                      dialogBox.center();
                      dialogBox.show();
                  }
              });
          }

          @Override
          protected Widget fillCell(Image widget, Contact model) {
              widget.setUrl(model.getUrlUserPic());
              return widget;
          }
      };
  }

  public ExampleTable getTable() {
      return table;
  };
}

