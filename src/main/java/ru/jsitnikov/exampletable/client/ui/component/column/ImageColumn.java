package ru.jsitnikov.exampletable.client.ui.component.column;

import com.google.gwt.user.client.ui.*;

/**
 * Created by jacob on 02.03.17.
 */
public abstract class ImageColumn<M>
        extends AbstractColumn<Label,Image, M> {

    @Override
    protected Widget createHeaderCellWidget() {
        return new Label();
    }

    @Override
    protected Widget fillHeaderCell(Label widget, String title) {
        widget.setText(title);
        return widget;
    }

    @Override
    protected Widget createCellWidget() {
        return new Image();
    }
}
