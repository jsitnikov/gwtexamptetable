package ru.jsitnikov.exampletable.client.ui.component.column;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Created by jacob on 02.03.17.
 */
public abstract class SimpleTextColumn <M>
        extends AbstractColumn<Label, Label, M> {

    @Override
    protected Widget createHeaderCellWidget() {
        return new Label();
    }

    @Override
    protected Widget fillHeaderCell(Label widget, String title) {
        widget.setText(title);
        return widget;
    }

    @Override
    public Widget createCellWidget() {
        return new Label();
    }
}
