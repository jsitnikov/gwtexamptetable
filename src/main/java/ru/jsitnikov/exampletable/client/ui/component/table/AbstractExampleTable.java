package ru.jsitnikov.exampletable.client.ui.component.table;

import com.google.gwt.user.client.ui.FlexTable;
import ru.jsitnikov.exampletable.client.ui.component.column.AbstractColumn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jacob on 02.03.17.
 */
public abstract class AbstractExampleTable<M> extends FlexTable implements INotify {

    private int HEADER_POSITION_ROW = 0;
    private final int DATA_POSITION_ROW = HEADER_POSITION_ROW + 1;

    private List<AbstractColumn> listColumns = new ArrayList<AbstractColumn>();
    private Map<Object, Integer> rowNumMap = new HashMap();
    private IKeyModel keyModel;

    public AbstractExampleTable() {
    }

    public AbstractExampleTable(IKeyModel keyModel) {
        this.keyModel = keyModel;
    }

    public void addColumn(AbstractColumn column, String text) {
        if(text != null) {
            column.setTextHeader(text);
        }
        listColumns.add(column);
    };

    public void setData(List<M> listModels) {
        renderTable(listModels);
    };

    public void refreshRow(M model) {
        if(rowNumMap.containsKey(getValueKeyModel(model))) {
            int row = rowNumMap.get(keyModel.getKey(model));
            renderRow(row, model);
        }
    }

    public void addRow(M model) {
        renderRow(this.getRowCount(), model);
    }

    public  void refresh(List<M> listModels) {
        renderTable(listModels);
    }

    public void setHeaderPositionRow(int positionRow) {
        HEADER_POSITION_ROW = positionRow;
    }

    private void renderTable(List<M> listModels) {
        clear();
        renderHeaderRow(HEADER_POSITION_ROW);
        int row = DATA_POSITION_ROW;
        for(M model : listModels) {
            renderRow(row++, model);
        }
    }

    private void renderHeaderRow(int row) {
        for(int col = 0; col < listColumns.size(); col++) {
            this.setWidget(row, col, listColumns.get(col).getHeaderCell());
        }
    }

    private void renderRow(int row, M model) {
        rowNumMap.put(getValueKeyModel(model), row);
        for(int col = 0; col < listColumns.size(); col++) {
            this.setWidget(row, col, listColumns.get(col).getCell(model));
        }

    }

    private Object getValueKeyModel(M model) {
        if(keyModel == null) {
            return model;
        }
        return keyModel.getKey(model);
    }
}
