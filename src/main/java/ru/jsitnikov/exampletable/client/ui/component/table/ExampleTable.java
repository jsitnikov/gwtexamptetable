package ru.jsitnikov.exampletable.client.ui.component.table;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;

/**
 * Created by jacob on 02.03.17.
 */
public class ExampleTable<M> extends AbstractExampleTable<M> {

    public ExampleTable() {
        super();
    }

    public ExampleTable(IKeyModel keyModel) {
        super(keyModel);
    }

    @Override
    public void notify(String text) {
        this.setHeaderPositionRow(1);
        this.setWidget(0,0, new Label(text));
        Window.alert(text);
    }
}
