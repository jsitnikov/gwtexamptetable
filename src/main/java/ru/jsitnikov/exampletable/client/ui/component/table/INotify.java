package ru.jsitnikov.exampletable.client.ui.component.table;

/**
 * Created by jacob on 03.03.17.
 */
public interface INotify {
    void notify(String text);
}
