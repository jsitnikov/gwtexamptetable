package ru.jsitnikov.exampletable.client.ui.component.table;

/**
 * Created by jacob on 03.03.17.
 */
public interface IKeyModel<T> {
    Object getKey(T item);
}
