package ru.jsitnikov.exampletable.client.ui.component.column;

import com.google.gwt.user.client.ui.Widget;

/**
 * Created by jacob on 02.03.17.
 */
public abstract class AbstractColumn<H extends Widget, C extends Widget, M> {

    private String textHeader;

    public Widget getHeaderCell() {
        return fillHeaderCell((H) createHeaderCellWidget(), textHeader);
    }

    public Widget getCell(M model) {
        C widget = (C) createCellWidget();
        addHandlerCell(widget, model);
        return fillCell(widget, model);
    }

    public void setTextHeader(String textHeader) {
        this.textHeader = textHeader;
    }

    protected void addHandlerCell(C widget, M model) {

    };

    protected abstract Widget createHeaderCellWidget();
    protected abstract Widget createCellWidget();
    protected abstract Widget fillHeaderCell(H widget, String title);
    protected abstract Widget fillCell(C widget, M model);

}
